defmodule Alpgrow.DataAccess do
  @moduledoc """
  Documentation for `Alpgrow.DataAccess`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Alpgrow.DataAccess.hello()
      :world

  """
  def hello do
    :world
  end
end
