
defmodule Alpgrow.DataAccess.Config.XandraNodes do
  use Skogsra.Type

  @impl Skogsra.Type
  @spec cast(String.t()) :: {:ok, [String.t()]} | :error
  def cast(value)

  def cast(""), do: :error

  def cast(value) when is_binary(value) do
    nodes =
      String.split(value, ",", trim: true)
      |> Enum.map(&String.trim/1)

    {:ok, nodes}
  end

  def cast(_) do
    :error
  end
end
