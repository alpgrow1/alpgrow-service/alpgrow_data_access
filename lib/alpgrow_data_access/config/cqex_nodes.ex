
defmodule Alpgrow.DataAccess.Config.CQExNodes do
  use Skogsra.Type

  @default_port 9042

  @impl Skogsra.Type
  @spec cast(String.t()) :: {:ok, [{String.t(), integer()}]} | :error
  def cast(value)

  def cast(""), do: :error

  def cast(value) when is_binary(value) do
    nodes =
      value
      |> String.split(",", trim: true)
      |> Enum.reduce_while([], fn host_port_str, acc ->
        trimmed_str = String.trim(host_port_str)

        with [host, port_str] <- String.split(trimmed_str, ":", parts: 2),
             {port, ""} <- Integer.parse(port_str) do
          {:cont, [{host, port} | acc]}
        else
          [host] ->
            {:cont, [{host, @default_port} | acc]}

          _ ->
            {:halt, :error}
        end
      end)

    case nodes do
      :error ->
        :error

      _ ->
        {:ok, nodes}
    end
  end

  def cast(_) do
    :error
  end
end
